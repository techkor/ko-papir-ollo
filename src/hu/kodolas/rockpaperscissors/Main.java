package hu.kodolas.rockpaperscissors;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

public class Main {

    public static final Object LOCK = new Object(), WAITING_FOR_PLAYER_LOCK
            = new Object();
    public static RockPaperScissors playerChoice, botChoice;
    public static int playerPoints, botPoints;

    private static final String TITLE = "Kő-papír-olló";
    private static final String LABEL_PLAYER = "Játékos";
    private static final String LABEL_BOT = "Bot";
    private static final String LABEL_RESET = "Új játék";
    private static final Dimension DEFAULT_SIZE = new Dimension(640, 120);
    private static final Font POINTS_FONT = new Font("Serif", Font.PLAIN, 16);
    private static final Font CHOICE_FONT = new Font("Serif", Font.BOLD, 32);

    private static Bot bot = new Bot();
    private static JFrame window;
    private static JLabel playerChoiceLabel, botChoiceLabel,
            playerPointsLabel, botPointsLabel;
    private static JButton resetButton;

    public static void main(String[] args) throws InterruptedException,
            IOException {
        createWindow();

        PrintStream botLog = new PrintStream(new FileOutputStream(
                "bot_choices.txt"));

        while (true) {

            synchronized (LOCK) {
                // Bot decides first
                try{
                    botChoice = bot.getNextChoice();
                }catch(Exception e)
                {
                    System.out.println("HIBA VAN A MATRIXBAN!!!");
                }
                botLog.println(botChoice);
                botLog.flush();

                // Wait for Player's choice
                waitForPlayer();
            }
            bot.playerChoice(playerChoice);
            updatePoints();
            updateWindow();
        }
        
    }

    private static void createWindow() {
        window = new JFrame(TITLE);
        window.setPreferredSize(DEFAULT_SIZE);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);

        window.setLayout(new GridLayout(3, 3));

        addComponents();
        setupKeyBindings();
        updateWindow();

        window.pack();
        window.setVisible(true);
    }

    private static void addComponents() {
        // First row
        window.add(new JLabel(LABEL_PLAYER, SwingConstants.CENTER));
        window.add(new JPanel());
        window.add(new JLabel(LABEL_BOT, SwingConstants.CENTER));

        // Second row
        playerPointsLabel = new JLabel("", SwingConstants.CENTER);
        playerPointsLabel.setFont(POINTS_FONT);
        window.add(playerPointsLabel);

        resetButton = new JButton(LABEL_RESET);
        resetButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                reset();
            }

        });
        window.add(resetButton);

        botPointsLabel = new JLabel("", SwingConstants.CENTER);
        botPointsLabel.setFont(POINTS_FONT);
        window.add(botPointsLabel);

        // Third row
        playerChoiceLabel = new JLabel("", SwingConstants.CENTER);
        playerChoiceLabel.setFont(CHOICE_FONT);
        window.add(playerChoiceLabel);

        window.add(new JPanel());

        botChoiceLabel = new JLabel("", SwingConstants.CENTER);
        botChoiceLabel.setFont(CHOICE_FONT);
        window.add(botChoiceLabel);
    }

    private static void setupKeyBindings() {
        InputMap inputMap = window.getRootPane().getInputMap(
                JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(KeyStroke.getKeyStroke("LEFT"), RockPaperScissors.ROCK);
        inputMap.put(KeyStroke.getKeyStroke("UP"), RockPaperScissors.PAPER);
        inputMap.put(KeyStroke.getKeyStroke("RIGHT"),
                RockPaperScissors.SCISSORS);

        ActionMap actionMap = window.getRootPane().getActionMap();
        actionMap.put(RockPaperScissors.ROCK, new SPSAction(
                RockPaperScissors.ROCK));
        actionMap.put(RockPaperScissors.PAPER, new SPSAction(
                RockPaperScissors.PAPER));
        actionMap.put(RockPaperScissors.SCISSORS, new SPSAction(
                RockPaperScissors.SCISSORS));
    }

    private static void waitForPlayer() throws InterruptedException {
        synchronized (WAITING_FOR_PLAYER_LOCK) {
            WAITING_FOR_PLAYER_LOCK.wait();
        }
    }

    private static void updatePoints() {
        if (playerChoice.defeats == botChoice)
            ++playerPoints;
        else if (playerChoice.defeatedBy == botChoice)
            ++botPoints;
    }

    private static void reset() {
        playerPoints = 0;
        botPoints = 0;
        playerChoice = null;
        botChoice = null;
        bot.reset();
        updateWindow();
    }

    private static void updateWindow() {
        playerPointsLabel.setText("" + playerPoints);
        botPointsLabel.setText("" + botPoints);

        playerChoiceLabel.setText(playerChoice == null ? "?" : playerChoice
                .toString());
        botChoiceLabel.setText(botChoice == null ? "?" : botChoice.toString());
    }

    private static RockPaperScissors getPlayerChoice() {
        return null;
    }

    private static class SPSAction extends AbstractAction {

        private final RockPaperScissors choice;

        public SPSAction(RockPaperScissors choice) {
            this.choice = choice;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            synchronized (WAITING_FOR_PLAYER_LOCK) {
                playerChoice = choice;
                WAITING_FOR_PLAYER_LOCK.notify();
            }
        }

    }
}
