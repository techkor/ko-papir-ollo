package hu.kodolas.rockpaperscissors;


public enum RockPaperScissors {
    ROCK, PAPER, SCISSORS;

    static {
        ROCK.name = "Kő";
        ROCK.defeats = SCISSORS;
        ROCK.defeatedBy = PAPER;
        ROCK.id = 0;

        PAPER.name= "Papir";
        PAPER.defeats = ROCK;
        PAPER.defeatedBy = SCISSORS;
        PAPER.id = 1;

        SCISSORS.name = "Olló";
        SCISSORS.defeats = PAPER;
        SCISSORS.defeatedBy = ROCK;
        SCISSORS.id = 2;
    }


    private String name;
    public RockPaperScissors defeats, defeatedBy;
    public int id;

    @Override
    public String toString() {
        return name;
    }

    public static RockPaperScissors getItemByID(int id){
        if (id == 0)
            return ROCK;
        else if (id == 1)
            return PAPER;
        else if (id == 2)
            return SCISSORS;
        return null;
    }
}

