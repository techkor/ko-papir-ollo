package hu.kodolas.rockpaperscissors;

public class Bot {

    public static final int SLIDING_WINDOW = 2;
    private static final int MODULO = calcModulo();
    private int history;
    private int[][] patterns = new int[MODULO][3];
    private int counter;

    // 3 ^ SLIDING_WINDOW
    private static int calcModulo() {
        int x = 1;
        for (int i = 0; i < SLIDING_WINDOW; ++i) {
            x *= 3;
        }
        return x;
    }

    public void playerChoice(RockPaperScissors playerChoice) {
        int id = playerChoice.id;
        if (counter >= SLIDING_WINDOW) {
            patterns[history][id]++;
        }
        counter++;

        history *= 3;
        history += id;
        history %= MODULO;
    }

    public RockPaperScissors getNextChoice() {
        int maxItemID = 0;
        for (int i = 0; i < 3; ++i) {
            if (patterns[history][i] > patterns[history][maxItemID]) {
                maxItemID = i;
            }
        }
        return RockPaperScissors.getItemByID(maxItemID).defeatedBy;
    }

    public void reset() {
        history = 0;
        counter = 0;
    }

}
